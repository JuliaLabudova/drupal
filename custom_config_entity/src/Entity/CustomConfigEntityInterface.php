<?php

namespace Drupal\custom_config_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Custom config entity entities.
 */
interface CustomConfigEntityInterface extends ConfigEntityInterface {

    /**
     * Get title.
     *
     * @return string
     */
    public function title();

    /**
     * Get description.
     *
     * @return string
     */
    public function description();
}
