<?php

/**
 * @file
 * Contains \Drupal\article_color\Plugin\Block\ArticleColor.
 */

namespace Drupal\article_color\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a 'Article: Block with color attribute' block.
 *
 * @Block(
 *   id = "article_color",
 *   admin_label = @Translation("Article: Block with color attribute")
 * )
 */
class ArticleColor extends BlockBase implements ContainerFactoryPluginInterface{

  /**
   * @var EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * ArticleColor constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $defaultEntity = $this->entityTypeManager->getListBuilder('default_entity');

    $items = [];
    $color = [];
    foreach ($defaultEntity->load() as $key => $entity) {
      if ($entity->getArticles()) {
        $currentColor = $this->entityTypeManager->getStorage('node')->load($entity->getArticles())
            ->get('field_new_article_color')->getValue();
        if ($currentColor) {
          $color[] = $currentColor[0]['value'];
        } else {
          $color[] = 'pink';
        }
      }
      $items[] = $entity->getName();
    }
    $list = [
      '#theme' => 'my_template',
      '#items' => $items,
    ];
    $list['#attached'] = [
      'library' => [
        'core/drupalSettings',
        'article_color/color.form'
      ],
      'drupalSettings' => [
        'color' => $color,
      ],
    ];

    return $list;
  }
}
