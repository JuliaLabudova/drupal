<?php

namespace Drupal\field_example\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * @FieldType(
 *   id = "some_new_fieldtype",
 *   label = @Translation("Some new fieldtype"),
 *   module = "field_example",
 *   description = @Translation("This field stores start date and end date."),
 *   default_widget = "some_new_fieldtype_w",
 *   default_formatter = "some_new_fieldtype_f"
 * )
 */
class DateField extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['start_date'] = DataDefinition::create('integer')
      ->setLabel(t('Start date'))
      ->setSetting('unsigned', TRUE);
    $properties['end_date'] = DataDefinition::create('integer')
      ->setLabel(t('End date'))
      ->setSetting('unsigned', TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'start_date' => [
          'type' => 'int'
        ],
        'end_date' => [
          'type' => 'int'
        ]
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
        'start_date' => '',
        'end_date' => ''
      ] + parent::defaultFieldSettings();
  }
}