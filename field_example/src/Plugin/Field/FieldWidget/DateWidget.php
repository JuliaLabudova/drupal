<?php

namespace Drupal\field_example\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Plugin implementation of the 'some_new_fieldtype_w' widget.
 *
 * @FieldWidget(
 *   id = "some_new_fieldtype_w",
 *   label = @Translation("Some new fieldtype - Widget"),
 *   description = @Translation("Some new fieldtype - Widget"),
 *   field_types = {
 *     "some_new_fieldtype"
 *   },
 *   multiple_values = TRUE,
 * )
 */
class DateWidget extends WidgetBase {

  const START_DATE = 'start_date';
  const END_DATE = 'end_date';

  const DATETIME = 'datetime';
  const TEXT_FIELD = 'textfield';

  /**
   * {@inheritdoc}
   */
  public function formElement(
      FieldItemListInterface $items,
      $delta,
      array $element,
      array &$form,
      FormStateInterface $form_state
  ) {
    $viewSetting = $this->getSetting('some_new_fieldtype_w');
    $default_start_value = isset($items[$delta]->start_date) ?
      $this->getDefaultValue($viewSetting, $items[$delta]->start_date) : '';
    $default_end_value = isset($items[$delta]->end_date) ?
      $this->getDefaultValue($viewSetting, $items[$delta]->end_date) : '';

    $element[self::START_DATE] = [
      '#type' => $viewSetting,
      '#title' => t('Start'),
      '#default_value' => $default_start_value,
      '#description' => t('Start date. Please, write in format YYYY-MM-DD HH:ii.'),
      '#ajax' => [
        'callback' => [$this, 'validateStartDateAjax'],
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
      '#element_validate' => [
        [
          $this,
          'validateElement',
        ],
      ],
    ];

    $element[self::END_DATE] = [
      '#type' => $viewSetting,
      '#title' => t('End'),
      '#default_value' => $default_end_value,
      '#description' => t('End date. Please, write in format YYYY-MM-DD HH:ii.'),
      '#ajax' => [
        'callback' => [$this, 'validateEndDateAjax'],
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
          ],
        ],
      '#element_validate' => [
        [
          $this,
          'validateElement',
        ],
      ],
      '#suffix' => '<span class="date-valid-message"></span>'
    ];

    return $element;
  }

    /**
     * @param array $element
     * @param FormStateInterface $form_state
     *
     * @return array
     */
    public function validateElement(array $element, FormStateInterface $form_state)
    {
      if (isset($element['start_date']['#value']['object'])) {
        $start_date = $element['start_date']['#value']['object']->getTimestamp();
        $end_date = $element['end_date']['#value']['object']->getTimestamp();
      } else {
        $start_date = strtotime($element['start_date']['#value']);
        $end_date = strtotime($element['end_date']['#value']);
      }

      if ($start_date >= $end_date) {
        $form_state->setError($element, t('It is not true.'));
      }

      return $element;
    }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $values = parent::massageFormValues($values, $form, $form_state);
    foreach ($values as $key => &$item) {
      if ($key == self::START_DATE || $key == self::END_DATE) {
        $item = $this->getDate($item);
      }
    }
    return $values;
  }

  /**
   * Get date in int format.
   *
   * @param $date
   * @return null|int
   */
  private function getDate($date)
  {
    $viewSetting = $this->getSetting('some_new_fieldtype_w');
    $formatDate = null;

    switch($viewSetting) {
      case self::DATETIME :
        if ($date instanceof DrupalDateTime) {
          $formatDate = $date->getTimestamp();
        }
        break;
      case self::TEXT_FIELD :
        if (!empty($date) && $this->isDate($date)) {
          $formatDate = strtotime($date);
        }
        break;
    }

    return $formatDate;
  }

  /**
   * @param string $date
   *
   * @return bool
   */
  private function isDate(string $date)
  {
    $regexp = '/^([\d]{4})[-]([\d]{2})[-]([\d]{2})[ ]([\d]{2})[:]([\d]{2})$/u';
    return preg_match($regexp, $date) ? true : false;
  }

  /**
   * @param bool $valid
   * @return AjaxResponse
   */
  private function getResponseForAjax(bool $valid)
  {
    if ($valid) {
      $css = ['border' => '1px solid green'];
      $message = $this->t('Start date is ok.');
    } else {
      $css = ['border' => '1px solid red'];
      $message = $this->t('Date is not OK.');
    }

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('.date-valid-message', $message));
    $response->addCommand(new CssCommand('.date-valid-message', $css));
    return $response;
  }

  /**
   * Get current value from date fields.
   *
   * @param FormStateInterface $form_state
   * @return array
   */
  public function getCurrentValue(FormStateInterface $form_state)
  {
    $viewSetting = $this->getSetting('some_new_fieldtype_w');
    $currentValue = [];

    switch ($viewSetting) {
      case self::DATETIME:
        $currentValue[self::START_DATE] =
          $form_state->getValue($this->fieldDefinition->getName())[self::START_DATE]['date'];
        $currentValue[self::END_DATE] =
          $form_state->getValue($this->fieldDefinition->getName())[self::END_DATE]['date'];
        break;
      case self::TEXT_FIELD:
        $currentValue[self::START_DATE] =
          $form_state->getValue($this->fieldDefinition->getName())[self::START_DATE];
        $currentValue[self::END_DATE] =
          $form_state->getValue($this->fieldDefinition->getName())[self::END_DATE];
        break;
    }

    return $currentValue;
  }

    /**
     * @param $start_date
     * @param $end_date
     * @param $field
     *
     * @return bool
     */
    public function isValidDate($start_date, $end_date, $field)
  {
    $viewSetting = $this->getSetting('some_new_fieldtype_w');

    switch($viewSetting) {
      case self::TEXT_FIELD :
        if (!$this->isDate($$field)) {
            return false;
        }
        $startDate = strtotime($start_date);
        $endDate = strtotime($end_date);
    }

    switch ($$field) {
      case self::START_DATE :
        if ($endDate == false) {
            return true;
        }
        break;
      case self::END_DATE :
        if ($startDate == false) {
            return true;
        }
        break;
    }

    if ($startDate < $endDate) {
      return true;
    }

    return false;
  }

  /**
   * Ajax callback to validate start date field.
   */
  public function validateStartDateAjax(array &$form, FormStateInterface $form_state) {
    $currentValue = $this->getCurrentValue($form_state);
    $valid = $this->isValidDate(
      $currentValue[self::START_DATE],
      $currentValue[self::END_DATE],
      self::START_DATE
    );

    return $this->getResponseForAjax($valid);
  }

  /**
   * Ajax callback to validate end date field.
   */
  public function validateEndDateAjax(array &$form, FormStateInterface $form_state) {
    $currentValue = $this->getCurrentValue($form_state);
    $valid = $this->isValidDate(
      $currentValue[self::START_DATE],
      $currentValue[self::END_DATE],
      self::END_DATE
    );

    return $this->getResponseForAjax($valid);
  }

  /**
   * Get default value for fields.
   *
   * @param string $settings
   * @param mixed $date
   * @return array
   */
  private function getDefaultValue(string $settings, $date)
  {
    switch ($settings) {
      case self::DATETIME :
        $date = DrupalDateTime::createFromTimestamp($date);
        break;
      case self::TEXT_FIELD :
        $date = date('Y-m-d H:i', $date);
        break;
    }
    return $date;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'some_new_fieldtype_w' => 'datetime',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['some_new_fieldtype_w'] = [
      '#title' => $this->t('Date field format'),
      '#type' => 'select',
      '#options' => [
        self::DATETIME => $this->t('Calendar'),
        self::TEXT_FIELD => $this->t('Text')
      ],
      '#default_value' => $this->getSetting('some_new_fieldtype_w'),
    ];

    return $form;
  }
}
