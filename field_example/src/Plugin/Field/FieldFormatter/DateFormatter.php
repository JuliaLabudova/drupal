<?php

namespace Drupal\field_example\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Plugin implementation of the 'some_new_fieldtype_f' formatter.
 *
 * @FieldFormatter(
 *   id = "some_new_fieldtype_f",
 *   label = @Translation("Some New Fieldtype - Formatter"),
 *   description = @Translation("Some New Fieldtype - Formatter"),
 *   field_types = {
 *     "some_new_fieldtype"
 *   }
 * )
 */
class DateFormatter extends FormatterBase{
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#markup' => 'Start date: ' . DrupalDateTime::createFromTimestamp($items[$delta]->start_date) .
          '<br>End date:' . DrupalDateTime::createFromTimestamp($items[$delta]->end_date)
      ];
    }

    return $elements;
  }
}
