<?php

namespace Drupal\custom_rest\Plugin\rest\resource;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Drupal\field_example\Plugin\Field\FieldWidget\DateWidget;
use Drupal\custom_entity\Entity\DefaultEntityInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a Custom Entity Resource
 *
 * @RestResource(
 *   id = "custom_entity_resource",
 *   label = @Translation("custom_entity_resource"),
 *   uri_paths = {
 *     "canonical" = "/custom_entities_api/custom_entity",
 *     "https://www.drupal.org/link-relations/create" = "/custom_entities_api/custom_entity"
 *   }
 * )
 */
class CustomEntityRestResource extends ResourceBase
{
  /**
   * @var EntityTypeManagerInterface
   */
  private $entityTypeManager;

    /**
     * @var Request
     */
  private $request;

    /**
     * CustomEntityRestResource constructor.
     *
     * @param array $configuration
     * @param string $plugin_id
     * @param mixed $plugin_definition
     * @param EntityTypeManagerInterface $entity_type_manager
     * @param LoggerInterface $serializer_formats
     * @param Request $request
     * @param LoggerInterface $logger
     */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    $serializer_formats,
    LoggerInterface $logger,
    Request $request
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $request);
    $this->entityTypeManager = $entity_type_manager;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Method for getting 'default_entity" entities.
   *
   * @return ResourceResponse
   */
  public function get()
  {
    $entities = $this->entityTypeManager->getStorage('default_entity');
    $date = $this->request->get('date');
    if (!$date) {
      foreach ($entities->loadMultiple() as $key => $entity) {
        $dateArray = $this->getValueFieldDate($entity);
        $json[] = $this->getDataForJson($entity, $key, $dateArray);
      }
    }

    if ($date) {
      foreach ($entities->loadMultiple() as $key => $entity) {
        $dateArray = $this->getValueFieldDate($entity);
        $current_date = strtotime($date);
        if (!$current_date) {
          throw new HttpException(406, 'Wrong date format!');
        }
        if ($dateArray[DateWidget::START_DATE] < $current_date
          && $dateArray[DateWidget::END_DATE] > $current_date
        ) {
          $json[] = $this->getDataForJson($entity, $key, $dateArray);
        }
      }
    }
    $response = ['message' => $json];
    return new ResourceResponse($response);
  }

    /**
     * Get values for field field_date.
     *
     * @param DefaultEntityInterface $entity
     * @return array
     */
    private function getValueFieldDate(DefaultEntityInterface $entity)
  {
    $date = [];
    if ($entity->get('field_date')) {
      $date[DateWidget::START_DATE] = $entity->get('field_date')->getValue()[0][DateWidget::START_DATE];
      $date[DateWidget::END_DATE] = $entity->get('field_date')->getValue()[0][DateWidget::END_DATE];
    }

    return $date;
  }

    /**
     * @param DefaultEntityInterface $entity
     * @param $key
     * @param $date
     *
     * @return array
     */
    private function getDataForJson(DefaultEntityInterface $entity, $key, $date)
  {
    if ($date[DateWidget::START_DATE]) {
      $json[$key][DateWidget::START_DATE] = date('Y-m-d H:i', $date[DateWidget::START_DATE]);
    }
    if ($date[DateWidget::END_DATE]) {
      $json[$key][DateWidget::END_DATE] = date('Y-m-d H:i', $date[DateWidget::END_DATE]);
    }
    $json[$key]['id'] = $entity->id();
    $json[$key]['langcode'] = $entity->getLangcode();

    $article_id = $entity->getArticle();
    if ($article_id) {
      $article = $this->entityTypeManager->getStorage('node')->load($article_id);
      $json[$key]['article']['id'] = $article->id();
      $json[$key]['article']['title'] = $article->getTitle();
    }

    return $json;
  }

  /**
   * Method for creating 'default_entity'
   *
   * @param array|null $data
   *
   * @return ModifiedResourceResponse
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function post($data) {
    $start_date = strtotime($data['start_date']);
    $end_date = strtotime($data['end_date']);

    if (!$start_date || !$end_date) {
        throw new HttpException(406, 'Wrong date format!');
    }
    $fieldDate = [
      0 =>
        ['start_date' => $start_date],
        ['end_date' => $end_date]
    ];

    $defaultEntity = $this->entityTypeManager->getStorage('default_entity')->create(
      [
        'name' => $data['name'],
        'custom_field' => $data['custom_field']
      ]
    );
    $defaultEntity->get('langcode')->setValue($data['langcode']);
    $defaultEntity->get('field_date')->setValue($fieldDate);

    if ($this->entityTypeManager->getStorage('node')->load((int)$data['article'])) {
        $defaultEntity->setArticle($data['article']);
    }

    try {
      $defaultEntity->save();
      return new ModifiedResourceResponse($defaultEntity, 201);
    } catch (EntityStorageException $e) {
      throw new HttpException(500, 'Internal Server Error', $e);
    }
  }
}
