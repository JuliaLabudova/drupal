<?php

namespace Drupal\custom_entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Defines a class to build a listing of Default entity entities.
 *
 * @ingroup custom_entity
 */
class DefaultEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Default entity ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity)
  {
      /* @var $entity \Drupal\custom_entity\Entity\DefaultEntity */
          $row['id'] = $entity->id();
          $row['name'] = Link::createFromRoute(
              $entity->label(),
              'entity.default_entity.edit_form',
              ['default_entity' => $entity->id()]
          );
          return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds()
  {
      $globalLangcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $query = $this->getStorage()->getQuery()
          ->condition('langcode', $globalLangcode)
          ->sort($this->entityType->getKey('id'));

      // Only add the pager if a limit is specified.
      if ($this->limit) {
          $query->pager($this->limit);
      }
      return $query->execute();
  }
}
